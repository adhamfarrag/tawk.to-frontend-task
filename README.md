### tawk.to Frontend Developer Test

#### Introduction

Hello Tawk.to Team 👋

For the test I decided to keep the stack as it is, thinking that the existing FE layers at Tawk.to are build using the same technologies (Relied on [Vue Telescope](https://www.vuetelescope.com/) browser extension and inspected CSS style to validate the information for https://dashboard.tawk.to/login).

The changes I made to the boilerplate were:
- Updated dependencies to the most updated versions I can use to keep the current stack as it is, for this I relied on [`Taze`](https://github.com/antfu/taze) CLI tool.
- I made changes to [Webpack configuration](./webpack.config.js) to fix hot reloading and websocket disconnection issues.
- Configuring a [Dev Container](./.devcontainer/devcontainer.json) with an official Microsoft Node 12 Image and some customization for VSCode. (Docker snd [VSCode Dev Contains Extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) are dependencies).
- Configured [ESLint](./.eslintrc.js).

<br>

#### Design Matching Notes
- Noticed minor differences between Figma and screenshots in the task document, my implementation is based on Figma file.
- As Figma I designed the search results following font and border design guidelines for article cards, linking it incremental links for categories.

<br>

#### Mock API Notes

- For DX purposes, I created a [utility file for APIs](./src/api.js) instead of calling them on demand inside the project.
- I couldn't see any use case for Authors API in designs, but included it in my API utility file.
- Created a search API built on top of the existing mock API inside webpack, taking a query and handling the search logic against title/content of articles.

<br>

#### Added modules:

- [screen-size-vue](https://npmjs.org/package/screen-size-vue) to help me create a [screen size indicator component](./src/components/DX/ScreenSizeIndicator.vue) to help me with CSS.
- [moment](https://npmjs.org/package/moment) to make it easier for me to create [utilities](./src/utils.js) for human readable date.
- [vuex-persistedstate](https://npmjs.org/package/vuex-persistedstate) because I needed to access Vuex Stores inside [category.vue](./src/pages/category.vue) page to guard the route anc check if a category is enabled or not.
- Eslint and esling plugins.

<br>


#### Unit testing:

- Used Jest for unit testing and focused on two Vue components and all utilities.


#### Getting Started:

Besides `npm run start` for lunching dev server, I added the following commands:

- `npm run lint` and `npm run lint:fix` for linting.
- `npm run test:unit` for running unit tests.

<br>
<br>

Finally, All good things.

Adham
