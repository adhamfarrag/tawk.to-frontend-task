module.exports = {
  parserOptions: {
    parser: '@babel/eslint-parser'
  },
  extends: [
    'plugin:vue/recommended',
    'standard'
  ],
  plugins: [
    'vue'
  ],
  rules: {
    'vue/v-on-style': 'off',
    'vue/v-bind-style': 'off',
    'vue/no-v-html': 'off',
    'max-len': 'off',
    'no-console': 'off',
    'no-useless-escape': 'off',
    'no-debugger': 'off',
    'vue/max-attributes-per-line': 'off',
    'vue/singleline-html-element-content-newline': 'off',
    'vue/multiline-html-element-content-newline': 'off',
    'vue/name-property-casing': ['error', 'PascalCase'],
    'vue/order-in-components': 'off',
    'no-undef': 'off'
  },
  ignorePatterns: ['node_modules/', 'dist/', 'public/', '*.md', '*.json']
}
