import axios from 'axios'

const baseURL = 'http://127.0.0.1:9000/api'

const fetchCategories = async () => {
  try {
    const categories = await axios.get(`${baseURL}/categories`)
    return categories.data
  } catch (error) {
    console.error(error)
  }
}

const fetchCategoryInfo = async (id) => {
  try {
    const categories = await axios.get(`${baseURL}/categories`)
    if (categories.data.length > 0) {
      const category = categories.data.find(category => category.id === id)
      return category
    }
  } catch (error) {
    console.error(error)
  }
}

const fetchCategoryArticles = async (id) => {
  try {
    const category = await axios.get(`${baseURL}/category/${id}`)
    return category.data
  } catch (error) {
    console.error(error)
  }
}

const fetchAuthor = async (id) => {
  try {
    const author = await axios.get(`${baseURL}/author/${id}`)
    return author.data
  } catch (error) {
    console.error(error)
  }
}

const searchKnowledgeBase = async (query) => {
  try {
    const results = await axios.get(`${baseURL}/search/${query}`)
    const data = results.data

    const cleanedQuery = query.trim().toLowerCase()

    const filteredData = data.filter(article => {
      const title = article.title.toLowerCase()
      const content = article.content.toLowerCase()
      return title.includes(cleanedQuery) || content.includes(cleanedQuery)
    })

    const output = filteredData.map(article => ({
      id: article.id,
      title: article.title
    }))

    return output
  } catch (error) {
    console.error(error)
  }
}

export {
  fetchCategories,
  fetchCategoryArticles,
  fetchAuthor,
  fetchCategoryInfo,
  searchKnowledgeBase
}
