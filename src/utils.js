import * as momentTemp from 'moment'

function timeAgo (dateString) {
  const moment = momentTemp.default
  const date = moment(dateString)
  const now = moment()

  const yearsAgo = now.diff(date, 'years')
  if (yearsAgo > 0) {
    return `${yearsAgo} years ago`
  }

  const monthsAgo = now.diff(date, 'months')

  if (monthsAgo > 0) {
    return `${monthsAgo} months ago`
  }

  const daysAgo = now.diff(date, 'days')
  return `${daysAgo} days ago`
}

// create a function that returns date with this format, june, 16 2021
function readableDate (dateString) {
  const moment = momentTemp.default
  const date = moment(dateString)
  return date.format('MMMM, DD YYYY')
}

function removeSpaces (str) {
  return str.replace(/\s/g, '')
}

export { timeAgo, removeSpaces, readableDate }
