import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import { fetchCategories } from '../api.js'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [
    createPersistedState({
      storage: window.sessionStorage
    })
  ],
  state: {
    categories: []
  },
  mutations: {
    setCategories (state, categories) {
      state.categories = categories
    }
  },
  actions: {
    async getSingleCategory (state, id) {
      if (this.state.categories.length === 0) {
        const categories = await fetchCategories()
        this.commit('setCategories', categories)
      }

      return this.state.categories.find((category) => category.id === id)
    }
  }
})
