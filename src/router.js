import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from './pages/index.vue'
import CategoryPage from './pages/category.vue'
import SearchPage from './pages/search.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', component: Index },
  { path: '/category/:categoryId', component: CategoryPage },
  { path: '/search', component: SearchPage }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
