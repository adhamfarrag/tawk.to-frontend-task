import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Vue from 'vue'
import ScreenSize from 'screen-size-vue'

import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

Vue.use(store)
Vue.use(ScreenSize)

new Vue({
  router,
  render: h => h(App),
  store
}).$mount('#app')
