import { mount } from '@vue/test-utils'
import HomeCard from '@/components/Category/HomeCard.vue'

describe('HomeCard', () => {
  it('renders correctly', () => {
    const category = {
      id: 'cat1',
      title: 'Getting Started',
      totalArticle: 10,
      updatedOn: '2019-12-11T03:02:10.297Z'
    }
    const wrapper = mount(HomeCard, {
      propsData: {
        category
      },
      stubs: ['router-link', 'router-view']
    })
    expect(wrapper.exists()).toBe(true)
  })

  it('renders correct category title', () => {
    const category = {
      id: 'cat1',
      title: 'Getting Started',
      totalArticle: 10,
      updatedOn: '2019-12-11T03:02:10.297Z'
    }
    const wrapper = mount(HomeCard, {
      propsData: {
        category
      },
      stubs: ['router-link', 'router-view']
    })
    expect(wrapper.find('h2').text()).toBe('Getting Started')
  })

  it('renders correct total article count', () => {
    const category = {
      id: 'cat1',
      title: 'Getting Started',
      totalArticle: 10,
      updatedOn: '2019-12-11T03:02:10.297Z'
    }
    const wrapper = mount(HomeCard, {
      propsData: {
        category
      },
      stubs: ['router-link', 'router-view']
    })
    expect(wrapper.find('#card-article-counter').text()).toBe('10 articles')
  })
})
