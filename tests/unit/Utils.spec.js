import moment from 'moment'
import { timeAgo, removeSpaces, readableDate } from '@/utils.js'

describe('timeAgo', () => {
  it('returns the correct time ago string for years', () => {
    const dateString = moment().subtract(2, 'years').format()
    expect(timeAgo(dateString)).toBe('2 years ago')
  })

  it('returns the correct time ago string for months', () => {
    const dateString = moment().subtract(3, 'months').format()
    expect(timeAgo(dateString)).toBe('3 months ago')
  })

  it('returns the correct time ago string for days', () => {
    const dateString = moment().subtract(5, 'days').format()
    expect(timeAgo(dateString)).toBe('5 days ago')
  })
})

describe('readableDate', () => {
  it('returns the date in "MMMM, DD YYYY" format', () => {
    const dateString = '2021-06-16'
    expect(readableDate(dateString)).toBe('June, 16 2021')
  })
})

describe('removeSpaces', () => {
  it('removes spaces from a string', () => {
    const str = 'remove spaces from this string'
    expect(removeSpaces(str)).toBe('removespacesfromthisstring')
  })

  it('handles string with no spaces', () => {
    const str = 'noSpacesHere'
    expect(removeSpaces(str)).toBe('noSpacesHere')
  })

  it('handles empty string', () => {
    const str = ''
    expect(removeSpaces(str)).toBe('')
  })
})
