import { mount, createLocalVue } from '@vue/test-utils'
import Search from '@/components/Search.vue'

// Mock the API function
jest.mock('@/api.js', () => ({
  searchKnowledgeBase: jest.fn().mockResolvedValue([
    { id: 'cat1', title: 'Category 1', content: 'Category 1 content' },
    { id: 'cat2', title: 'Category 2', content: 'Category 2 content' },
    { id: 'cat3', title: 'Category 3', content: 'Category 3 content' },
    { id: 'cat4', title: 'Category 4', content: 'Category 4 content' },
    { id: 'cat5', title: 'Category 5', content: 'Category 5 content' }
  ])
}))

describe('Search', () => {
  let wrapper

  beforeEach(() => {
    const localVue = createLocalVue()
    wrapper = mount(Search, {
      localVue,
      stubs: ['router-link', 'router-view']
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('displays search results when query length > 3', async () => {
    const input = wrapper.find('#search-input')
    await input.setValue('content')
    wrapper.setData({ isResultsVisible: true })
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#search-results-container').exists()).toBe(true)
  })

  it('hides search results when clicking outside', async () => {
    wrapper.setData({ isResultsVisible: true })
    document.body.click()
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.isResultsVisible).toBe(false)
  })

  it('hides search results and clears searchQuery when result clicked', async () => {
    const input = wrapper.find('#search-input')
    await input.setValue('test')
    await input.trigger('focus')
    await wrapper.vm.$nextTick() // Wait for DOM updates
    const resultItem = wrapper.find('.search-result-item')
    await resultItem.trigger('click')
    expect(wrapper.vm.isResultsVisible).toBe(false)
    expect(wrapper.vm.searchQuery).toBe('')
  })
})
